<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/reset.css">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <title>DossTer</title>
    <?php
      // Affiche les erreurs
      ini_set('display_errors', 1);
         ini_set('display_startup_errors', 1);
         error_reporting(E_ALL);

      $extensions = [];
      $liste_fichiers = array("path"=>[], "basename"=>[], "dirname"=>[], "extension"=>[], "profondeur"=>[], "classes"=>[]);

      ////récupère le nom de tous les fichiers de l'arborescence, incluant les sous-dossiers
      function getSubDirectories($dir)
      {
          $subDir = array();
          //// Get and add directories of $dir
          //// $directories = array_filter(glob($dir), 'is_dir');
          $directories = glob($dir);
          $subDir = array_merge($subDir, $directories);
          //// Foreach directory, recursively get and add sub directories
          foreach ($directories as $directory) {
              $subDir = array_merge($subDir, getSubDirectories($directory.'/*'));
          }
          //// Return list of sub directories
          return $subDir;
      }

      ////pretty print pour les arrays
      function pr($var)
      {
          print '<pre>';
          print_r($var);
          print '</pre>';
      }

    ?>
  </head>

  <body>
    <?php
      ////choix du dossier + afficher son nom
      $dir='dossier_exemple';
      // $dir='portrait-robot';
      echo '<h1>'.$dir.'</h1>';

      $alors = getSubDirectories($dir);

      ////enlève le nom du dossier principal au début de la liste
      array_shift($alors);

      ////crée une array associative qui récupère les infos nécessaires pour chaque fichier
      foreach ($alors as $file) {
          $infos_fichier = pathinfo($file);
          array_push($liste_fichiers['path'], $file);
          array_push($liste_fichiers['basename'], $infos_fichier['basename']);
          array_push($liste_fichiers['dirname'], $infos_fichier['dirname']);
          if (is_dir($file)) {
              array_push($liste_fichiers['extension'], 'DIR');
          } else {
              if (!isset($infos_fichier['extension'])) {
                  array_push($liste_fichiers['extension'], null);
              } else {
                  array_push($liste_fichiers['extension'], $infos_fichier['extension']);
              }
          }

          $profondeur = substr_count($file, '/')-1;
          array_push($liste_fichiers['profondeur'], $profondeur);
      }

      ////trie les fichiers dans l'ordre alphabétique
      asort($liste_fichiers['dirname']);
      // $bon_ordre = array_keys($liste_fichiers['dirname']);

      foreach ($liste_fichiers['dirname'] as $i => $dirname) {
          ////on remplace les espaces dans les noms de dossier par des tirets, afin de respecter les spécifications du CSS
          $dossier = str_replace(' ', '-', $dirname);
          preg_match('|/*.*/(.*)$|', $dossier, $matches);
          if (isset($matches[1])) {
              $liste_fichiers["classes"][$i] = $matches[1];
          } else {
              $liste_fichiers["classes"][$i] = 'racine';
          }
      }

      // ////crée une section pour séparer le titre du contenu
      ////version <p>
      echo '<section id="racine">';
      foreach ($liste_fichiers["dirname"] as $k =>$dirname) {
          $element = $liste_fichiers["basename"][$k];

          if (is_dir($liste_fichiers["path"][$k])) {
              echo '<div class="dossier '.$liste_fichiers["classes"][$k].'" id="'.str_replace(' ', '-', $element).'"><h2>'.$element.'</h2></div>';
          } else {
              echo '<p class="'.$liste_fichiers["classes"][$k].'">'.$element.'</p>';
          }
      }
      echo '<div id="racine"></div>';
      echo '</section>';
    ?>
    <script type="text/javascript" src="script/script.js"></script>
  </body>
</html>
