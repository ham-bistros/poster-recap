////range chaque élément dans son dossier parent
function rangement(elements) {
  for (var el of elements) {
    classe = el.getAttribute('class');
    if (el.tagName == 'DIV') {
      classe = classe.replace('dossier ', '');
    }
      document.getElementById(classe).appendChild(el);
  }
}

function sorting(container){
  var element = container.children;
  element = Array.prototype.slice.call(element);
  element.sort(function(a, b) {
    if (a.textContent < b.textContent) {
      return -1;
    } else {
      return 1;
    }
  });
  container.innerHTML = "";

  for(var i = 0, l = element.length; i < l; i++) {
    container.appendChild(element[i]);}
  }

function remonte(query){
  elements = document.querySelectorAll(query);
  for (var el of elements){
    var parent = el.parentNode;
    parent.prepend(el);
  }
}

var p = document.getElementsByTagName('p');
// var p = document.getElementsByTagName('span');
var dossiers = document.getElementsByClassName('dossier');

////c'est tout rangé maintenant
rangement(p);
rangement(dossiers);

for (var doss of dossiers){
  sorting(doss);
}

sorting(document.getElementsByTagName('section')[0]);

remonte(".dossier");
remonte(".dossier h2");
